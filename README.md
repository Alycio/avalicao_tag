# Comparador de avaliacoes #


## Requirements ##

* Python 3.5+
* Virtul Env

### How do I get set up? ###

__Create a virtualenv to isolate our package dependencies locally:__

* virtualenv env
* source env/bin/activate

__Install dependencies into the virtualenv:__

* pip install -r requirements.txt
* python manage.py migrate

__Run project:__

* python manage.py runserver

__Run goodreads sync task__:

In other tab run the comand:

* python manage.py process_tasks
