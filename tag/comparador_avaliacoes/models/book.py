from django.db import models


class Book(models.Model):

    id = models.AutoField(primary_key=True)
    isbn = models.CharField(max_length=13)
    name = models.CharField(max_length=50)
    edition = models.DateField()
    tag_rating = models.DecimalField(max_digits=3, decimal_places=2)
    goodreads_rating = models.DecimalField(max_digits=3, decimal_places=2, null=True)
