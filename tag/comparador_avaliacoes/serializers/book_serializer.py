from tag.comparador_avaliacoes.models import Book
from rest_framework import serializers

class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('isbn', 'name', 'edition', 'tag_rating', 'goodreads_rating')