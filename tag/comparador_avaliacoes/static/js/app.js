(function(){
    'use strict';


    var app = new Vue({
        el: '#app',
        methods: {
            getData: function() {
                this.$set('loader', true);
                this.$http.get('/books/list').then(
                    function sucessCallback(data) {
                        this.$set('books', data.body);
                        this.toggleLoader();
                    }, function errorCallback (error) {
                        console.log(error);
                        this.$set('loader', false);
                        this.toggleLoader();
                    }
                );
            },
            toggleLoader: function() {
                this.$set('loader', false);
            }
        },
        ready: function() {
            this.getData()
        }
    });

}())
