from tag.comparador_avaliacoes.models import Book
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from tag.comparador_avaliacoes.serializers.book_serializer import BookSerializer
from tag.comparador_avaliacoes.services import BookService
from rest_framework.decorators import api_view
import json

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

class BookView(APIView):
    serializer_class = BookSerializer

    def get(self, request):
        books = BookService.get_books_edition_order()
        serializer = BookSerializer(books, many=True)

        return Response(serializer.data)
