from django.apps import AppConfig


class ComparadorAvaliacoesConfig(AppConfig):
    name = 'comparador_avaliacoes'
