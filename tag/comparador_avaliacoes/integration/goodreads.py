import requests
import json
from tag import settings

URL = settings.GOODREADS_URL
KEY = settings.GOODREADS_USER_KEY

class GoodReads():

    def get_request_good_reads(context, params):
        url = URL + context
        params['key']= KEY
        return requests.get(url, params)
    
    def get_review_by_isbns(isbns):
        context = 'book/review_counts.json'
        params = {  
            'format': 'json',
            'isbns': isbns
        }
        response = GoodReads.get_request_good_reads(context, params)
        return response.json()
