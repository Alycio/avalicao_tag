from background_task import background
from tag.comparador_avaliacoes.services import BookService

@background()
def update_books_goodreads_rating():
    BookService.update_books_goodreads_rating()
