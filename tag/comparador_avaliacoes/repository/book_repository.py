from ..models.book import Book
from tag import settings
import json

class BookRepository():
    
    def get_all_book():
        try:
            book = Book.objects.all()
        except Book.DoesNotExist:
            book = None
        return book

    def get_book_by_isbn(isbn_code):
        try:
            book = Book.objects.filter(isbn=isbn_code)[0]
        except:
            book = None
        return book

    def get_initial_charge():
        return BookRepository.__load_books_from_json()

    def update_goodreads_rating_by_isbn(isbn_code, average_rating):
        Book.objects.filter(isbn=isbn_code).update(goodreads_rating=average_rating)

    def __load_books_from_json():
        with open(settings.TAG_JSON_NAME) as tag_json_file:
            tag_json_str = tag_json_file.read()
            return json.loads(tag_json_str) 

    def get_books_edition_order(order): 
        # book = Book.objects.all().order_by('-edition')
        books = BookRepository.get_all_book()
        books = BookRepository.sort_by_edition(books)
        if order == "desc":
            return books[::-1]
        else:
            return books
        
    def sort_by_edition(array):
        less = []
        equal = [] 
        greater = []
        if len(array) > 1: 
            pivot = array[len(array)-1]
            for element in array:
                if element.edition < pivot.edition:
                    less.append(element)   
                if element.edition == pivot.edition:
                    equal.append(element)
                if element.edition > pivot.edition:
                    greater.append(element)
            less = BookRepository.sort_by_edition(less)
            greater = BookRepository.sort_by_edition(greater)
            return less + equal + greater
        else:
            return array