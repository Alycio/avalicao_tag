from tag.comparador_avaliacoes.integration import GoodReads

class GoodreadsService():
    
    def get_reviews_to_comparison(books):
        isbns = GoodreadsService.__isbns_from_books_list(books)
        return GoodReads.get_review_by_isbns(isbns)

    def __isbns_from_books_list(books):
        isbns = []
        for book in books:
            isbns.append(int(book.isbn))

        isbns_str = str(isbns)[1:-1]
        isbns_str = isbns_str.replace(" ", "")

        return isbns_str