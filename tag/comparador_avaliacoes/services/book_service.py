from ..models.book import Book
from tag.comparador_avaliacoes.repository import BookRepository
from tag.comparador_avaliacoes.services import GoodreadsService, DateService
from django.db import transaction

class BookService():

    @transaction.atomic()
    def charge_database():
        tag_books_dict = BookRepository.get_initial_charge() 
        for book_dict in tag_books_dict['results']:
            if book_dict is not None: 
                if 'isbn' in book_dict:
                    isbn = book_dict['isbn']
                else:
                    continue
                book = BookRepository.get_book_by_isbn(isbn)
                if book is None:
                    book = Book()
                book.isbn = isbn
                if 'numRatings' in book_dict and 'totalRatings' in book_dict:
                    book.tag_rating = BookService.__get_tag_book_average(book_dict['numRatings'], book_dict['totalRatings'])
                if 'name' in book_dict:
                    book.name = book_dict['name']
                if 'edition' in book_dict:
                    book.edition = DateService.parse_date(book_dict['edition'])

                book.save()
        BookService.update_books_goodreads_rating()

    @transaction.atomic()
    def update_books_goodreads_rating():
        books = BookService.get_all_book()
        books_goodreads = GoodreadsService.get_reviews_to_comparison(books)
        for book_dict in books_goodreads['books']:
            isbn = book_dict['isbn13']
            average_rating = book_dict['average_rating'] 
            BookRepository.update_goodreads_rating_by_isbn(isbn, average_rating)

    def get_all_book():
        return list(BookRepository.get_all_book())

    def get_book_by_isbn():
        return BookRepository.get_book_by_isbn()

    def get_books_edition_order():
        return BookRepository.get_books_edition_order("desc")

    def __get_tag_book_average(num_ratings, total_rating):
        return round(total_rating/num_ratings, 2)