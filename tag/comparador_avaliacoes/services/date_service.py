from datetime import datetime

MONTH_PORTUGUESE_DICT = {
    'Janeiro': '1',
    'Fevereiro': '02',
    'Março': '03',
    'Abril': '04',
    'Maio': '05',
    'Junho': '06',
    'Julho': '07',
    'Agosto': '08',
    'Setembro': '09',
    'Outubro': '10',
    'Novembro': '11',
    'Dezembro': '12'
}

class DateService():

    def parse_date(str_date):
        date_list = str_date.split()
        date_month = MONTH_PORTUGUESE_DICT[date_list[0]]
        date_year = date_list[-1]
        new_str_date = "{date_year}-{date_month}".format(date_year=date_year, date_month=date_month)

        return datetime.strptime(new_str_date, '%Y-%m')
