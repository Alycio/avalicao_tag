certifi==2017.7.27.1
chardet==3.0.4
Django==1.11.4
django-background-tasks==1.1.11
django-compat==1.0.14
djangorestframework==3.6.4
idna==2.6
pytz==2017.2
requests==2.18.4
six==1.10.0
urllib3==1.22
